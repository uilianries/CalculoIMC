/*!
 * @file TestPaciente.java
 * @brief Testes para a classe Paciente
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */

package org.ita.imc;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestPaciente {

    @Test
    public void testBaixoPesoMuitoGrave () {
        Paciente paciente = new Paciente(35, 1.50);
        assertEquals(15.6, paciente.calcularIMC(), 0.5);
        assertEquals("Baixo peso muito grave", paciente.diagnostico());
    }

    @Test
    public void testBaixoPesoGrave () {
        Paciente paciente = new Paciente(40, 1.55);
        assertEquals(16.6, paciente.calcularIMC(), 0.5);
        assertEquals("Baixo peso grave", paciente.diagnostico());
    }

    @Test
    public void testBaixoPeso () {
        Paciente paciente = new Paciente(42, 1.55);
        assertEquals(17.5, paciente.calcularIMC(), 0.5);
        assertEquals("Baixo peso", paciente.diagnostico());
    }

    @Test
    public void testPesoNormal () {
        Paciente paciente = new Paciente(67, 1.71);
        assertEquals(22.9, paciente.calcularIMC(), 0.5);
        assertEquals("Peso normal", paciente.diagnostico());
    }

    @Test
    public void testSobrepeso () {
        Paciente paciente = new Paciente(65, 1.60);
        assertEquals(25.4, paciente.calcularIMC(), 0.5);
        assertEquals("Sobrepeso", paciente.diagnostico());
    }

    @Test
    public void testObesidadeGrauI () {
        Paciente paciente = new Paciente(95, 1.72);
        assertEquals(32.1, paciente.calcularIMC(), 0.5);
        assertEquals("Obesidade grau I", paciente.diagnostico());
    }

    @Test
    public void testObesidadeGrauII () {
        Paciente paciente = new Paciente(103.56, 1.67);
        assertEquals(37.1, paciente.calcularIMC(), 0.5);
        assertEquals("Obesidade grau II", paciente.diagnostico());
    }

    @Test
    public void testObesidadeGrauIII () {
        Paciente paciente = new Paciente(228.3, 1.84);
        assertEquals(67.4, paciente.calcularIMC(), 0.5);
        assertEquals("Obesidade grau III (obesidade mórbida)", paciente.diagnostico());
    }
}

