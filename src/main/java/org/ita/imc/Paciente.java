/*!
 * @file Paciente.java
 * @brief Representação para calculo de IMC
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.imc;

/*!
 * @brief Abstração de uma paciente com valor de IMC
 */
public class Paciente {

  private double m_peso;   //< Peso do paciente em quilos
  private double m_altura; //< Altura do paciente em metros

  /*!
   * @brief Preenche dados para calculo de IMC
   * @param peso    Peso do paciente, em quilos
   * @param altura  Altura do paciente em metros
   */
  public Paciente(double peso, double altura) {
    m_peso = peso;
    m_altura = altura;
  }

  /*!
   * @brief Getter peso
   * @return Peso do paciente em quilos
   */
  public double peso() {
    return m_peso;
  }

  /*!
   * @brief Getter altura
   * @return Altura do paciente em metros
   */
  public double altura() {
    return m_altura;
  }

  /*!
   * @brief Aplica calculo de IMC sobre atributos do paciente
   * @return Resultado IMC
   */
  public double calcularIMC() {
    return m_peso / (m_altura * m_altura);
  }

  /*!
   * @brief Executa diagnóstico sobre IMC do paciente
   * @return Faixa de enquadramento do IMC do paciente
   */
  public String diagnostico() {
    double imc = calcularIMC();
    String faixa;

    if (imc < 16.0) {
      faixa = "Baixo peso muito grave";
    } else if (imc < 17.0) {
      faixa = "Baixo peso grave";
    } else if (imc < 18.5) {
      faixa = "Baixo peso";
    } else if (imc < 25.0) {
      faixa = "Peso normal";
    } else if (imc < 30.0) {
      faixa = "Sobrepeso";
    } else if (imc < 35.0) {
      faixa = "Obesidade grau I";
    } else if (imc < 40.0) {
      faixa = "Obesidade grau II";
    } else {
      faixa = "Obesidade grau III (obesidade mórbida)";
    }

    return faixa;
  }
  
  /**
   * To String
   */
   public String toString() {
       return "Peso: " + m_peso + " Altura: " + m_altura;
   }

}
