/*!
 * @file Principal.java
 * @brief Execução para calculo de IMC
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.imc;

import java.text.DecimalFormat;

/*!
 * @brief Executa o calculo de 3 pacientes
 */
public class Principal {

  // Formarta precição de double para 2 casas decimais
  private static DecimalFormat decimalFormat = new DecimalFormat(".##");

  /*!
   * @brief Instancia 3 pacientes e imprime suas informações
   */
  public static void main(String [] args) {
    Paciente pacientes[] = { new Paciente(66.0, 1.70), new Paciente(103.5, 1.80), new Paciente(85.0, 1.75)};

    for (Paciente paciente : pacientes) {
      System.out.println("Paciente possui peso de " + paciente.peso() + " quilos e altura de " + paciente.altura() + " metros.");
      System.out.println("Seu IMC é de " + decimalFormat.format(paciente.calcularIMC()) + " kg/m²"); 
      System.out.println("Sua faixa foi classificada como " + paciente.diagnostico() + "\n");
    }
  }
}
